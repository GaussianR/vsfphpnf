<?php

namespace UI;

defined('RENDERCALL') OR exit('No direct script access allowed');

require_once 'form_helper.php';
?>

<?php

/**
 * Renders navigation bar.
 * @param array $_params Contains needed params to render the view.
 */
function Navigation($_params = array())
{
  extract($_params);
  ?>


  <nav class="navbar navbar-default">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-search-collapse" aria-expanded="false">
        <span class="sr-only"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= \Config::GetBaseURL() ?>">Inicio</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-search-collapse">
      <ul class="nav navbar-nav">
        <li <?= $section == 'news/create' ? 'class="active"' : '' ?>>
          <a href="<?= \Config::GetBaseURL('news.php/create') ?>">Nueva noticia</a>
        </li>
      </ul>

      <!-- Search box -->
      <?php
      $search_categories = array(
          'pattern'  => 'Todos',
          'author'   => 'Autor',
          'keywords' => 'Palabras clave',
      );
      ?>
      <div id="search-form" class="navbar-form navbar-right" role="search">
        <div class="form-group">
          <?= Form_Dropdown('name', $search_categories, array($search_category), 'class="btn btn-default dropdown-toggle" data-toggle="dropdown"') ?>
          <div class="btn-group">
            <input type="text" class="form-control" placeholder="Buscar término" value="<?= $search_term ?>">
            <span class="search-clear glyphicon glyphicon-remove-circle" <?= $search_term != '' ? '' : 'style="display: none;"' ?>></span>
          </div>
        </div>
        <button type="submit" class="btn btn-default">
          <span class="glyphicon glyphicon-search"></span>
        </button>
      </div>

    </div><!-- /.navbar-collapse -->
  </nav>

  <?php
}
?>

<?php

/**
 * Renders pagination widget.
 * @param array $_params Contains needed params to render the view.
 */
function Pagination($_params = array())
{
  extract($_params);
  ?>

  <!-- HTML Code for pagination layer -->

  <?php
}
?>


<?php

/**
 * Renders a image loader modal.
 * @param type $id Node HTML id
 */
function ImageModal($id)
{
?>

    <div id="<?= $id ?>" class="modal in" aria-hidden="false" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Seleccionar imagen</h4>
          </div>
          <div class="modal-body">
            <div class="form-group note-group-select-from-files">
              <label>Seleccionar desde los archivos</label>
              <input class="note-image-input form-control" type="file" name="files" accept="image/*" max-size="2097152">
            </div>
            <div class="form-group" style="overflow:auto;">
              <label>URL de la imagen</label>
              <input class="note-image-url form-control col-md-12" type="text">
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-primary note-image-btn disabled" disabled="">Aplicar</button>
          </div>
        </div>
      </div>
    </div

<?php
}
?>

