<?php

/**
 * Renders a dropdown ui control.
 * @param string $name UI Control name field.
 * @param array $options Array of items.
 * @param array $selected Selected items.
 * @param string $extra Additional fields for the UI control.
 */
function Form_Dropdown($name, $options, $selected = array(), $extra = '')
{
  ?>
  <select name="<?= $name ?>" <?= $extra ?>>
    <?php foreach ($options as $key => $value): ?>
    <option value="<?= $key ?>" <?= array_key_exists($key, $selected) ? 'selected="selected"': '' ?>><?= $value ?></option>
    <?php endforeach; ?>
  </select>
  <?php
}
?>

