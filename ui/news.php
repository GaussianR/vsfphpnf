<?php

namespace UI;

defined('RENDERCALL') OR exit('No direct script access allowed');

require_once 'extras.php';
?>

<?php

/**
 * Renders news summary sub-view in order to embed it within news list view.
 * @return View content rendered
 * @param array $_params Contains needed params to render the view.
 */
function News_Summary($_params = array())
{
  extract($_params);
  ob_start();

  $link = \Config::GetBaseURL('news.php/view/' . $id);
  ?>

  <div class="row">
    <div class="col-sm-3">
      <a href="<?= $link ?>">
        <img class="img-thumbnail" src="<?= $image ?>" alt="<?= $title ?>" width="240" height="240">							
      </a>
    </div>
    <div class="col-sm-9">

      <!-- New title -->
      <h2>
        <a href="<?= $link ?>"><?= $title ?></a>
      </h2>


      <p class="post-meta">
        <!-- New creation date -->
        Publicado por <a href="<?= \Config::GetBaseURL('search.php/author/' . $author) ?>" title="<?= $author ?>" alt="<?= $author ?>" rel="author"><?= $author ?></a> el <b><?= date('j M Y', strtotime($created)) ?></b> 

        <!-- Edit button -->
        <a data-toggle="tooltip" title="Editar noticia" alt="Editar noticia" class="pull-right" href="<?= \Config::GetBaseURL('news.php/edit/' . $id) ?>"><i class="glyphicon glyphicon-pencil"></i></a>  
      </p>
      <p></p>
      <?= $summary ?>
      <p></p>
      <div class="new-controls">
        <a href="<?= $link ?>" class="btn btn-primary pull-right"><span>Leer más</span></a>
      </div>
    </div>
  </div>

  <?php
  $result = ob_get_contents();
  ob_end_clean();

  return $result;
}
?>

<?php
/**
 * Renders news list view.
 * @param array $_params Contains needed params to render the view.
 * @return View content rendered
 * @note Both index.php and search.php controllers render this view.
 */
function News_List($_params = array())
{
  extract($_params);
  ob_start();
?>

<?php foreach ($news as $new): ?>
  <?= News_Summary($new) ?>
  <hr>
<?php endforeach; ?>
  
<!-- Pagination -->
<div class="row">
  <div class="col-md-12 text-center">
    <nav>
      <?php Pagination(array(/* ... */)); ?>
    </nav>
  </div>
</div>

<?php
  $result = ob_get_contents();
  ob_end_clean();
  
  return $result;
}
?>

<?php
/**
 * Renders new detail view.
 * @param array $_params Contains needed params to render the view.
 * @return View content rendered
 */
function News_Detail($_params = array())
{
  extract($_params);
  ob_start();
?>

  <h1><?= $title ?>
    <div class="panel-body">
      <!-- New creation date -->
      <small>
        Publicado por <a href="<?= \Config::GetBaseURL('search.php/author/' . $author) ?>" title="<?= $author ?>" alt="<?= $author ?>" rel="author"><?= $author ?></a> el <b><?= date('j M Y', strtotime($created)) ?></b>
        <!-- Edit button -->
        <a data-toggle="tooltip" title="Editar noticia" alt="Editar noticia" class="pull-right" href="<?= \Config::GetBaseURL('news.php/edit/' . $id) ?>"><i class="glyphicon glyphicon-pencil"></i></a>
      </small>
    </div>
  </h1>
  <div class="panel-body">
    <?= $content ?>
  </div>

  <?php if ($updated != NULL): ?>
    <!-- Modification date -->
    <h3 class="text-right">
      <small>Última modificatión el <b><?= date('j M Y', strtotime($updated)) ?></b> a las <b><?= date('H:i', strtotime($updated)) ?></b></small>
    </h3>
  <?php endif; ?>

  <hr>

  <div class="panel-body">
    <p>Palabras clave: 
      <?php foreach (explode(' ', $keywords) as $keyword): ?>
        &nbsp;<a href="<?= \Config::GetBaseURL('search.php/keywords/' . $keyword) ?>" alt="<?= $keyword ?>"><span class="label label-default"><span class="glyphicon glyphicon-tag"></span> <?= $keyword ?></span></a>
      <?php endforeach; ?>
    </p>
  </div>
  
<?php
  $result = ob_get_contents();
  ob_end_clean();
  
  return $result;
}
?>

<?php
/**
 * Renders edit/create view.
 * @param array $_params Contains needed params to render the view.
 * @return View content rendered.
 */
function News_Edit($_params = array())
{
  extract($_params);
  ob_start();
?>


<form class="form-horizontal" action="<?= \Config::GetBaseURL($id == -1 ? 'news.php/create' : 'news.php/edit/' . $id) ?>" method="POST" role="form">  
  <div class="row">
    
     <!-- Image dialog -->
    <?php ImageModal('image-modal'); ?>
    
    <!-- Image container -->
    <div class="col-md-4 text-center">
      <div class="img-hover">
        <img id="image-preview" class="img-thumbnail img-hover-image" src="<?= $image ?>" alt="<?= $title ?>" width="240" height="240">
        <input id="new-image" type="hidden" name="image" value="<?= $image ?>">

        <div class="img-hover-body" data-toggle="modal" data-target="#image-modal">
          <h1><span class="glyphicon glyphicon-option-horizontal"></span></h1>
        </div>
      </div>
    </div>

    <div class="col-md-8">

      <!-- Title -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="new-title">Título:</label>
        <div class="col-sm-10">
          <input class="form-control" value="<?= $title ?>" id="new-title" name="title" placeholder="Introduce un título" required>
        </div>
      </div>
      
      <!-- Author -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="new-author">Autor:</label>
        <div class="col-sm-10">
          <input class="form-control" value="<?= $author ?>" id="new-author" name="author" placeholder="Introduce un autor" required>
        </div>
      </div>
      
      <!-- Summary -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="new-summary">Resumen:</label>
        <div class="col-sm-10"> 
          <textarea class="form-control" id="new-summary" name="summary" required></textarea>
        </div>
      </div>

      <!-- Keywords -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="new-keywords">Términos:</label>
        <div class="col-sm-10">
          <input class="form-control summernote" value="<?= $keywords ?>" id="new-keywords" name="keywords" data-role="tagsinput" required>
        </div>
      </div>
    </div>
  </div>
  <hr>

  <!-- Content -->
  <div class="row">
    <div class="panel-body"> 
      <textarea class="form-control summernote" id="new-content" name="content"></textarea>
    </div>
  </div>

  <div class="form-group panel-body pull-right"> 
    <button type="submit" class="btn btn-default"><?= $id == -1 ? 'Añadir' : 'Modificar' ?></button>
  </div>
</form>

<script type="text/javascript">
  function edit_postinit()
  {
    $('#new-summary').summernote('code', <?= json_encode($summary) ?>);
    $('#new-content').summernote('code', <?= json_encode($content) ?>);
  }
</script>
  
<?php
  $result = ob_get_contents();
  ob_end_clean();
  
  return $result;
}
?>

<?php
/**
 * Renders not found news view.
 * @return View content rendered.
 */
function News_NotFound()
{
  ob_start();
?>

<div class="alert alert-danger">
  No se ha encontrado ninguna noticia.
</div>
  
<?php
  $result = ob_get_contents();
  ob_end_clean();
  
  return $result;
}
?>