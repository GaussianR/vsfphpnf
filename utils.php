<?php

defined('RENDERCALL') OR exit('No direct script access allowed');

require_once 'config.php';

/**
 * Util class for controllers.
 */
class ControllerUtils
{
  /**
   * Checks if the resquest is a POST.
   * @return Boolean value.
   */
  public static function IsPostRequest()
  {
    return $_SERVER['REQUEST_METHOD'] == 'POST';
  }
  
  /**
   * Checks if the resquest is a GET.
   * @return Boolean value.
   */
  public static function IsGetRequest()
  {
    return $_SERVER['REQUEST_METHOD'] == 'GET';
  }
  
  /**
   * Obtains a request field, either from $_POST array or $_GET array.
   * @param string $key Field Name.
   * @param string $default The default value in case given field name doesn't exist.
   * @return string
   */
  public static function GetResquestField($key, $default = NULL)
  {
    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
      return isset($_GET[$key]) ? $_GET[$key] : $default;
    }
    else if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
      return isset($_POST[$key]) ? $_POST[$key] : $default;
    }
    
    return $default;
  }
  
  /**
   * Obtains page offset.
   * @return int Page offset.
   */
  public static function GetPage()
  {
    if (!isset($_GET['page']))
    {
      return 0;
    }

    if (!ctype_digit($_GET['page']))
    {
      return -1;
    }

    return intval($_GET['page']);
  }
  
  /**
   * Obtains an uri segment 
   * @param int $idx Segment index
   * @param string $default Default value in case the segment doesn't exist
   * @return string 
   */
  public static function GetUriSegment($idx, $default = NULL)
  {
    $segments = explode('/', $_SERVER['PATH_INFO']);
    return count($segments) > $idx ? $segments[$idx] : $default;
  }
}
