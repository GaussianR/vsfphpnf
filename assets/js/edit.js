
function edit_change_image(url)
{
  $('#image-preview').attr("src", url);
  $('#new-image').val(url);
}

$(document).ready(function ()
{
  $('#new-summary').summernote({
    lang: 'es-ES',
    height: 100,
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['fontname', ['fontname']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['insert', ['link', 'video']],
      ['view', ['codeview', 'help']]
    ],
  });

  $('#new-content').summernote({
    lang: 'es-ES',
    height: 200,
  });


  $('#image-modal .note-image-url').on('keyup', function ()
  {
    if ($(this).val() == "")
    {
      $('#image-modal .note-image-btn').addClass('disabled').prop('disabled', true);
    }
    else
    {
      $('#image-modal .note-image-btn').removeClass('disabled').prop('disabled', false);
    }
  });

  $('#image-modal .note-image-input').on('change', function ()
  {
    $.extend(new FileReader(),
    {
      onload: function (e)
      {
        edit_change_image(e.target.result);
        $('#image-modal').modal('hide');
      },
      onerror: function () 
      {
        deferred.reject(this);
      }
    }).readAsDataURL(this.files[0]);

  });

  $('#image-modal .note-image-btn').click(function ()
  {
    edit_change_image($('#image-modal .note-image-url').val());
  });

  edit_postinit();
});