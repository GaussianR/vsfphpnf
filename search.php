<?php

define('RENDERCALL', TRUE);

require_once 'utils.php';
require_once 'config.php';
require_once 'db/news.php';

require_once 'ui/body.php';
require_once 'ui/news.php';

/**
 * Search controller class.
 */
class SearchController
{
  /**
   * Class constructor.
   */
  public function __construct()
  {
    $this->m_Db = new NewsModel(\Config::GetDatabase());
  }
  
  /**
   * Controller processor function,
   */
  public function Process()
  {
    $action = ControllerUtils::GetUriSegment(1);
    if(!array_key_exists($action, self::$s_ActionsHandler))
    {
      header('Refresh:0;url='.\Config::GetBaseURL());
      return;
    }
    
    
    $term = ControllerUtils::GetUriSegment(2);
    $news = !empty($term) ?
            $this->m_Db->{self::$s_ActionsHandler[$action]}( 
            $term, (object) array(
                        'offset' => ControllerUtils::GetPage(),
                        'count'  => \Config::Get('pagination')['per_page']
            )) : NULL;
    
    $params['search_term'] = $term;
    $params['search_category'] = $action;

    \UI\Body(array(
        'title' => \Config::Get('page_title'),
        'view'  => $news == NULL ? \UI\News_NotFound() :
                \UI\News_List(array('news' => $news)),
    ));
  }

  protected $m_Db = NULL;
  
  protected static $s_ActionsHandler = array(
      'author' => 'GetByAuthor',
      'keywords' => 'GetByKeywords',
      'pattern' => 'GetByPattern',
  );
}
?>

<?php

//Call the controller
$controller = new SearchController();
$controller->Process();

?>