<?php

define('RENDERCALL', TRUE);

require_once 'utils.php';
require_once 'config.php';
require_once 'db/news.php';

require_once 'ui/body.php';
require_once 'ui/news.php';

/**
 * Main controller class.
 */
class MainController
{
  /**
   * Class constructor.
   */
  public function __construct()
  {
    $this->m_Db = new NewsModel(\Config::GetDatabase());
  }
  
  /**
   * Controller processor function,
   */
  public function Process()
  {
    $news = $this->m_Db->GetLasts((object)array(
        'offset' => ControllerUtils::GetPage(),
        'count' => \Config::Get('pagination')['per_page']
    ));
    
    \UI\Body(array(
    'title' => \Config::Get('page_title'),
    'view' => $news == NULL ? \UI\News_NotFound() : 
                              \UI\News_List(array('news' => $news)),
    ));
  }
  
  protected $m_Db = NULL;
}
?>

<?php
//Call the controller
$controller = new MainController();
$controller->Process();
?>