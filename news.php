<?php

define('RENDERCALL', TRUE);

require_once 'utils.php';
require_once 'config.php';
require_once 'db/news.php';

require_once 'ui/body.php';
require_once 'ui/news.php';

/**
 * News controller class.
 */
class NewsController
{

  /**
   * Class constructor.
   */
  public function __construct()
  {
    $this->m_Db = new NewsModel(\Config::GetDatabase());
  }

  /**
   * Load edit dependencies.
   * @params array params Body params.
   */
  protected function LoadEditDependencies(&$params)
  {
    $params['js_dependencies'] = array(
        \Config::GetBaseURL('assets/js/tagsinput.js'),
        'http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.0/summernote.js',
        \Config::GetBaseURL('assets/js/summernote-es-ES.js'),
        \Config::GetBaseURL('assets/js/edit.js'),
    );

    $params['css_dependencies'] = array(
        \Config::GetBaseURL('assets/css/tagsinput.css'),
        'http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.0/summernote.css',
    );
  }
  
  /**
   * Controller processor function.
   */
  public function Process()
  {
    $action = ControllerUtils::GetUriSegment(1);
    if(!array_key_exists($action, self::$s_ActionsHandler))
    {
      header('Refresh:0;url='. \Config::GetBaseURL());
      return;
    }
   
    $params = $this->{self::$s_ActionsHandler[$action]}();
    if(!empty($params))
    {
      \UI\Body($params);
    }
  }
  
  /**
   * Method invoked for action 'edit'.
   */
  public function EditNew()
  {
    $id = intval(ControllerUtils::GetUriSegment(2));
    if(ControllerUtils::IsPostRequest())
    {
      $this->m_Db->Update($id, 
              ControllerUtils::GetResquestField('title'), 
              ControllerUtils::GetResquestField('author'), 
              ControllerUtils::GetResquestField('summary'), 
              ControllerUtils::GetResquestField('content'),
              ControllerUtils::GetResquestField('keywords'),
              ControllerUtils::GetResquestField('image'));
      
      header('Refresh:0;url='. \Config::GetBaseURL('news.php/view/' . $id));
      return NULL;
    }
    
    $new = $this->m_Db->Get($id);
    $params['title'] = $new == NULL ? \Config::Get('page_title') : $new['title'];
    $params['view'] = $new == NULL ? \UI\News_NotFound() : \UI\News_Edit($new);
    
    $this->LoadEditDependencies($params);
    return $params;
  }

  /**
   * Method invoked for action 'create'.
   */
  public function CreateNew()
  {
    if(ControllerUtils::IsPostRequest())
    {
      $id =   $this->m_Db->Create( 
              ControllerUtils::GetResquestField('title'), 
              ControllerUtils::GetResquestField('author'), 
              ControllerUtils::GetResquestField('summary'), 
              ControllerUtils::GetResquestField('content'),
              ControllerUtils::GetResquestField('keywords'),
              ControllerUtils::GetResquestField('image'));
      
      header('Refresh:0;url='. \Config::GetBaseURL('news.php/view/' . $id));
      return NULL;
    }
    
    $params['section'] = 'news/create';
    $params['title'] = \Config::Get('page_title');
    $params['view'] = \UI\News_Edit(array(
        'id' => -1,
        'title' => '',
        'author' => '',
        'summary' => '',
        'content' => '',
        'keywords' => '',
        'image' => \Config::Get('news_default_image'),
        
    ));
    
    $this->LoadEditDependencies($params);
    return $params;
  }
  
  /**
   * Method invoked for action 'create'.
   */
  public function ViewNew()
  {
    $new = $this->m_Db->Get(intval(ControllerUtils::GetUriSegment(2)));
    $params['title'] = $new == NULL ? \Config::Get('page_title') : $new['title'];
    $params['view'] = $new == NULL ? \UI\News_NotFound() : \UI\News_Detail($new);
    
    return $params;
  }
  
  /**
   * 
   * @var NewsModel Database model.
   */
  protected $m_Db = NULL;
  
  /**
   *
   * @var array Action to method mapper.
   */
  protected static $s_ActionsHandler = array(
      'edit'    => 'EditNew',
      'create'  => 'CreateNew',
      'view' => 'ViewNew',
  );

}
?>

<?php

//Call the controller
$controller = new NewsController();
$controller->Process();
?>
