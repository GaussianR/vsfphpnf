<?php

require_once 'config.php';

defined('RENDERCALL') OR exit('No direct script access allowed');

mysqli_report(MYSQLI_REPORT_OFF);

class NewsModel
{

  /**
   * NewsModel constructor.
   * @param object $config Contains all the connection parameters.
   */
  public function __construct($config)
  {
    $this->m_Connection = new mysqli($config->host, $config->username, $config->password, $config->database);
    if ($this->m_Connection->connect_error)
    {
      exit('Couldn\'t obtain connection to the database.<br>Error (' .
                      $this->m_Connection->connect_errno . '): ' .
                      $this->m_Connection->connect_error) . '<br>' .
              defined('DEBUG') ? nl2br((new Exception())->getTraceAsString()) : '';
    }

    if (!$this->m_Connection->set_charset($config->char_set))
    {
      $this->RaiseError();
    }
  }

  /**
   * Shows MYSQL error and ends script execution.
   */
  protected function RaiseError()
  {
    exit('Database error (' . $this->m_Connection->errno . '): ' .
                    $this->m_Connection->error) . '<br>' .
            defined('DEBUG') ? nl2br((new Exception())->getTraceAsString()) : '';
  }

  /**
   * Format query extras this way ORDER BY <field> <DESC|ASC> [LIMIT <offset>, <count>]
   * @param array $limits Limits of the query.
   * @param array $sorting Sorting parameters of the query.
   * @return boolean|string Either the query extras string or FALSE if query
   *         offset < 0.
   */
  protected function FormatQueryExtras($limits, $sorting)
  {
    if ($sorting == NULL)
    {
      $sorting = Config::GetSorting();
    }

    $extra = ' ORDER BY ' . $sorting->field . ' ' . $sorting->order;

    if ($limits != NULL)
    {
      if ($limits->offset < 0)
      {
        return FALSE;
      }

      $extra.=' LIMIT ' . $limits->offset . ', ' . $limits->count;
    }
    
    return $extra;
  }

  /**
   * Obtain the number of news.
   * @return int Number of news.
   */
  public function GetCount()
  {
    $result = $this->m_Connection->query('SELECT COUNT(*) FROM `news`');
    if ($result === FALSE)
    {
      return $this->RaiseError();
    }

    return intval($result->fetch_row()[0]);
  }

  /**
   * Obtains last news.
   * @param array $limits Limits of the query.
   * @param array $sorting Sorting parameters of the query.
   * @return array Last news.
   */
  public function GetLasts($limits = NULL, $sorting = NULL)
  {
    if (($extras = $this->FormatQueryExtras($limits, $sorting)) === FALSE)
    {
      return NULL;
    }
    
    $result = $this->m_Connection->query('SELECT * FROM `news`' . $extras);
    if ($result === FALSE)
    {
      return $this->RaiseError();
    }
    
    return $result->fetch_all(MYSQLI_ASSOC);
  }

  /**
   * Obtains the number of news filtered by authors.
   * @param string $author Author to filter.
   * @return int Number of news filtered by author.
   */
  public function GetByAuthorCount($author)
  {
    $author = $this->m_Connection->real_escape_string($author);
    $result = $this->m_Connection->query('SELECT COUNT(*) FROM `news` '
            . 'WHERE author LIKE \'%' . $author . '%\'');

    if ($result === FALSE)
    {
      return $this->RaiseError();
    }

    return intval($result->fetch_row()[0]);
  }

  /**
   * Obtains news filtered by author.
   * @param string $author Author to filter.
   * @param array $limits Limits of the query.
   * @param array $sorting Sorting parameters of the query.
   * @return array News filtered by author.
   */
  public function GetByAuthor($author, $limits = NULL, $sorting = NULL)
  {
    if (($extras = $this->FormatQueryExtras($limits, $sorting)) === FALSE)
    {
      return NULL;
    }
    
    $author = $this->m_Connection->real_escape_string($author);
    $result = $this->m_Connection->query('SELECT * FROM `news`'
            . 'WHERE author LIKE \'%' . $author . '%\'' . $extras);
    
    if ($result === FALSE)
    {
      return $this->RaiseError();
    }
    
    return $result->fetch_all(MYSQLI_ASSOC);
  }
  
  /**
   * Obtains the number of news filtered by keywords.
   * @param string $keywords Keywords to filter.
   * @return int Number of news filtered by keywords.
   */
  public function GetByKeywordsCount($keywords)
  {
    $keywords = $this->m_Connection->real_escape_string($keywords);
    $result = $this->m_Connection->query('SELECT COUNT(*) FROM `news` WHERE '
            . 'MATCH (keywords) AGAINST (\'' . $keywords . '\' IN BOOLEAN MODE)');

    if ($result === FALSE)
    {
      return $this->RaiseError();
    }

    return intval($result->fetch_row()[0]);
  }

  /**
   * Obtains news filtered by keywords.
   * @param string $keywords Keywords to filter.
   * @param array $limits Limits of the query.
   * @param array $sorting Sorting parameters of the query.
   * @return array News filtered by keywords.
   */
  public function GetByKeywords($keywords, $limits = NULL, $sorting = NULL)
  {
    if (($extras = $this->FormatQueryExtras($limits, $sorting)) === FALSE)
    {
      return NULL;
    }
    
    $keywords = $this->m_Connection->real_escape_string($keywords);
    $result = $this->m_Connection->query('SELECT * FROM `news` WHERE '
            . 'MATCH (keywords) AGAINST (\'' . $keywords . '\' IN BOOLEAN MODE)'
            . $extras);
    
    if ($result === FALSE)
    {
      return $this->RaiseError();
    }
    
    return $result->fetch_all(MYSQLI_ASSOC);
  }

  /**
   * Obtains the number of news filtered by keywords|author.
   * @param string $pattern Term to filter.
   * @return int Number of news filtered by keywords|author.
   */
  function GetByPatternCount($pattern)
  {
    $pattern = $this->m_Connection->real_escape_string($pattern);
    $result = $this->m_Connection->query('SELECT COUNT(*) FROM `news` WHERE '
            . 'author LIKE \'%' . $pattern . '%\' OR'
            . ' MATCH (keywords) AGAINST (\'' . $pattern . '\' IN BOOLEAN MODE)');

    if ($result === FALSE)
    {
      return RaiseError();
    }

    return intval($result->fetch_row()[0]);
  }

  /**
   * Obtains news filtered by keywords|author.
   * @param string $pattern Term to filter.
   * @param array $limits Limits of the query.
   * @param array $sorting Sorting parameters of the query.
   * @return array News filtered by keywords|author.
   */
  public function GetByPattern($pattern, $limits = NULL, $sorting = NULL)
  {    
    if (($extras = $this->FormatQueryExtras($limits, $sorting)) === FALSE)
    {
      return NULL;
    }
    
    $pattern = $this->m_Connection->real_escape_string($pattern);
    $result = $this->m_Connection->query('SELECT * FROM `news` WHERE '
            . 'author LIKE \'%' . $pattern . '%\' OR'
            . ' MATCH (keywords) AGAINST (\'' . $pattern . '\' IN BOOLEAN MODE)'
            . $extras);
    
    if ($result === FALSE)
    {
      return $this->RaiseError();
    }
    
    return $result->fetch_all(MYSQLI_ASSOC);
  }

  /**
   * Obtains the new that matches given id.
   * @param int $id Id to filter.
   * @return array New that matches given id.
   */
  public function Get($id)
  {    
    $result = $this->m_Connection->query('SELECT * FROM `news` WHERE id = ' . $id);
    
    if ($result === FALSE)
    {
      return $this->RaiseError();
    }
    
    return $result->fetch_array(MYSQLI_ASSOC);
  }

  /**
   * Updates the new that matches given id
   * @param int $id
   * @param string $title
   * @param string $author
   * @param string $summary
   * @param string $content
   * @param string $keywords Keywords 
   * @param string $image
   * @return Boolean TRUE if modifications has been made, otherwise FALSE.
   */
  public function Update($id, $title, $author, $summary, $content, $keywords, $image)
  {
    $result = $this->m_Connection->query('UPDATE `news` SET '
            . 'title = \'' . $this->m_Connection->real_escape_string($title) . '\', '
            . 'author = \'' . $this->m_Connection->real_escape_string($author) . '\', '
            . 'summary = \'' . $this->m_Connection->real_escape_string($summary) . '\', '
            . 'content = \'' . $this->m_Connection->real_escape_string($content) . '\', '
            . 'keywords = \'' . $this->m_Connection->real_escape_string($keywords) . '\', '
            . 'image = \'' . $this->m_Connection->real_escape_string($image) . '\', '
            . 'updated = CURRENT_TIMESTAMP WHERE id = ' . $id);
    
    if ($result === FALSE)
    {
      return $this->RaiseError();
    }
    
    return $this->m_Connection->affected_rows == 1;
  }

  /**
   * Creates a new
   * @param string $title
   * @param string $author
   * @param string $summary
   * @param string $content
   * @param string $keywords
   * @param string $image
   * @return int Id of the inserted new.
   */
  function Create($title, $author, $summary, $content, $keywords, $image)
  {
    $result = $this->m_Connection->query(
    'INSERT INTO `news` (title, author, summary, content, keywords, image) VALUES(\''
    . $this->m_Connection->real_escape_string($title) . '\', \''
    . $this->m_Connection->real_escape_string($author) . '\', \'' 
    . $this->m_Connection->real_escape_string($summary) . '\', \'' 
    . $this->m_Connection->real_escape_string($content) . '\', \'' 
    . $this->m_Connection->real_escape_string($keywords) . '\', \'' 
    . $this->m_Connection->real_escape_string($image) . '\')');
      
    if ($result === FALSE)
    {
      return $this->RaiseError();
    }
    
    return $this->m_Connection->insert_id;
  }

  /**
   *
   * @var object Database MYSQLi link.
   */
  protected $m_Connection = NULL;
}

?>